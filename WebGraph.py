# -*- coding: utf-8 -*-
import os, webbrowser, time

class WebGraph:
    def __init__(self):
        pass

    def openWebGraph(self, browserPath, graphPath):
        # Open WebGraph
        webbrowser.get(browserPath).open('file://' + os.path.realpath(graphPath), 2)

    def openNeo4JBrowser(self, browserPath, Neo4JBrowserPath):
        webbrowser.get(browserPath).open(Neo4JBrowserPath, 2)