# -*- coding: utf-8 -*-
from py2neo import *
from Config import *
import os

class DBHandler:
    def __init__(self):
        self.config = Config()
        
        # Connect to Neo4j server
        self.graph = Graph(ip_addr = self.config.getNeo4jServerBolt(), username = self.config.getNeo4jUsername(), password = self.config.getNeo4jPassword())
        self.cwd = os.getcwd()

        # Statement to load csv data into the neo4j database
        self.loadCSVQuery = '''LOAD CSV WITH HEADERS FROM {csvfile} AS line 
        MERGE (subj:Subject{Name:line.subject}) 
        MERGE (obj:Object{Name:line.object}) 
        MERGE (subj) -[:TO {Rel:line.relation}]-> (obj)'''
        
        # Query to empty the database
        self.emptyDBQuery = '''MATCH (n)-[r]->(m) DELETE n,r,m;'''

    # Load data from CSV file to Neo4j. Use following command
    # MATCH (n)-[r]->(m) RETURN n,r,m;
    # to see all nodes and relationship in NEO4J browser
    def emptyDB(self):
        self.graph.run(self.emptyDBQuery)
        
    def populateDB(self):
        self.graph.run(self.loadCSVQuery, csvfile = "file:///" + self.cwd + "\csvDump.csv")