Procedure for setting up CoreNLP Server and Python Application Layer

Make sure the following requirements are met :

Java JDK 8 minimum

Python 3.5 minimum
You will need these dependencies:
Pip
NLTK
Tika
py2neo

Make sure the following environment variables are set :

%JAVA_HOME% = C:\Progra~1\Java\jdk1.8.0_201

Path = 

C:\Program Files\apache-maven-3.6.0\bin
C:\Users\user\AppData\Local\Programs\Python\Python37-32 (or your distribution)
C:\Users\user\AppData\Local\Programs\Python\Python37-32\Scripts (bis)
%JAVA_HOME%\bin

1. Download Stanford CoreNLP here : https://stanfordnlp.github.io/CoreNLP/#about

2. Follow the instructions here : https://github.com/stanfordnlp/CoreNLP
or alternately follow these : Download and install Maven here : https://maven.apache.org/
2.1 Make sure to include Maven in your path
2.2 Move to the CoreNLP directory.
2.3 Execute the following comand : mvn package
2.4 Troubleshoot until successful

3. Still in the CoreNLP directory, execute the following command to start the server :
java -mx4g -cp "*" edu.stanford.nlp.pipeline.StanfordCoreNLPServer -port 9000 -timeout 15000

You can now access the server on the following address : http://localhost:9000/

Refer to Stanford CoreNLP Documentation for API usage

4. Download and install Neo4j Desktop
4.1 Start application, create new graph
4.2 Set password: s0mep4ssw0rd
4.3 Click on manage->settings and comment this line to allow for Load CSV from external source : dbms.directories.import=import

5. Change directory to ./ParseToGraph
5.1 Execute : python mapConceptsAndRelations.py
5.2 Follow instructions

And voila !
