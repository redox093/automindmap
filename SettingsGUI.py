# -*- coding: utf-8 -*-

import sys
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
import ctypes
from Config import *
import tkinter as tk
from tkinter import filedialog
 
class SettingsGUI(QMainWindow):
 
    def __init__(self):
        super().__init__()

        self.title = 'Parse 2 Graph - Settings'
        self.left = 100
        self.top = 100
        self.width = 900
        self.height = 600

        self.config = Config()

        self.initUI()
 
    def initUI(self):

        # Window title, icon and geometry
        self.setWindowTitle(self.title)
        self.setWindowIcon(QIcon('images/mindmap3.png'))
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.setFixedSize(self.width, self.height)


        # Windows taskbar icon
        self.appid = 'parse2graphicon'
        ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(self.appid)
        
        # Main Menu
        mainMenu = self.menuBar()
        fileMenu = mainMenu.addMenu('Fichier')
        
        # Sub Menu
        exitButton = QAction(QIcon('images/exit.png'), 'Quitter', self)
        exitButton.setShortcut('Ctrl+Q')
        exitButton.setStatusTip("Quitter l'application")
        exitButton.triggered.connect(self.close)
        fileMenu.addAction(exitButton)

        # Labels
        self.labelNLPServer = QLabel(self)
        self.labelNLPServer.setGeometry(QRect(160, 100, 200, 15))
        self.labelNLPServer.setObjectName("labelNLPServer")
        self.labelNLPServer.setText("Addresse IP du serveur NLP")

        self.labelNeo4jServer = QLabel(self)
        self.labelNeo4jServer.setGeometry(QRect(380, 100, 200, 15))
        self.labelNeo4jServer.setObjectName("labelNeo4jServer")
        self.labelNeo4jServer.setText("Addresse Bolt du serveur Neo4j")

        self.labelNeo4jHttp = QLabel(self)
        self.labelNeo4jHttp.setGeometry(QRect(370, 200, 200, 15))
        self.labelNeo4jHttp.setObjectName("labelNeo4jHttp")
        self.labelNeo4jHttp.setText("Addresse Http du serveur Neo4j")

        self.labelNeo4jUser = QLabel(self)
        self.labelNeo4jUser.setGeometry(QRect(410, 290, 200, 15))
        self.labelNeo4jUser.setObjectName("labelNeo4jUser")
        self.labelNeo4jUser.setText("Nom d'utilisateur")

        self.labelNeo4jPassword = QLabel(self)
        self.labelNeo4jPassword.setGeometry(QRect(410, 380, 200, 15))
        self.labelNeo4jPassword.setObjectName("labelNeo4jPassword")
        self.labelNeo4jPassword.setText("Mot de passe")

        self.labelBrowser = QLabel(self)
        self.labelBrowser.setGeometry(QRect(600, 100, 200, 15))
        self.labelBrowser.setObjectName("labelBrowser")
        self.labelBrowser.setText("Localisation du navigateur")

        # Line Edits
        self.lineEditNLPServer = QLineEdit(self)
        self.lineEditNLPServer.setGeometry(QRect(140, 130, 180, 50))
        self.lineEditNLPServer.setObjectName("lineEditNLPServer")

        self.lineEditNeo4JServerBolt = QLineEdit(self)
        self.lineEditNeo4JServerBolt.setGeometry(QRect(360, 130, 180, 50))
        self.lineEditNeo4JServerBolt.setObjectName("lineEditNeo4JServerBolt")

        self.lineEditNeo4JServerHttp = QLineEdit(self)
        self.lineEditNeo4JServerHttp.setGeometry(QRect(360, 220, 180, 50))
        self.lineEditNeo4JServerHttp.setObjectName("lineEditNeo4JServerHttp")

        self.lineEditNeo4JUser = QLineEdit(self)
        self.lineEditNeo4JUser.setGeometry(QRect(360, 310, 180, 50))
        self.lineEditNeo4JUser.setObjectName("lineEditNeo4JUser")

        self.lineEditNeo4JPassword = QLineEdit(self)
        self.lineEditNeo4JPassword.setGeometry(QRect(360, 400, 180, 50))
        self.lineEditNeo4JPassword.setObjectName("lineEditNeo4JPassword")

        self.lineEditBrowserPath = QLineEdit(self)
        self.lineEditBrowserPath.setGeometry(QRect(580, 130, 180, 50))
        self.lineEditBrowserPath.setObjectName("lineEditBrowserPath")

        # Buttons
        self.saveChangesBtn = QPushButton(self)
        self.saveChangesBtn.setGeometry(QRect(170, 500, 250, 50))
        self.saveChangesBtn.setObjectName("saveChangesBtn")
        self.saveChangesBtn.setText("    Enregistrer les modifications")
        self.saveChangesBtn.setIcon(QIcon('images/check.png'))

        self.setDefaultBtn = QPushButton(self)
        self.setDefaultBtn.setGeometry(QRect(470, 500, 250, 50))
        self.setDefaultBtn.setObjectName("setDefaultBtn")
        self.setDefaultBtn.setText("    Rétablir les paramètres par défaut")
        self.setDefaultBtn.setIcon(QIcon('images/restore.png'))

        self.setBrowserPathBtn = QPushButton(self)
        self.setBrowserPathBtn.setGeometry(QRect(580, 225, 180, 50))
        self.setBrowserPathBtn.setObjectName("setBrowserPathBtn")
        self.setBrowserPathBtn.setText("   Sélectionner le navigateur")
        self.setBrowserPathBtn.setIcon(QIcon('images/browser.png'))

        self.saveChangesBtn.clicked.connect(self.saveChanges)
        self.setDefaultBtn.clicked.connect(self.restoreDefaults)
        self.setBrowserPathBtn.clicked.connect(self.chooseBrowser)

        self.showConfig()
        
        self.show()

    def showConfig(self):
        self.lineEditNeo4JServerBolt.setText(self.config.getNeo4jServerBolt())
        self.lineEditNeo4JServerHttp.setText(self.config.getNeo4jServerHttp())
        self.lineEditNeo4JUser.setText(self.config.getNeo4jUsername())
        self.lineEditNeo4JPassword.setText(self.config.getNeo4jPassword())
        self.lineEditNLPServer.setText(self.config.getNLPServerHttp())
        self.lineEditBrowserPath.setText(self.config.getBrowserPath())
 
    def saveChanges(self):
        self.config.setNeo4jServerBolt(self.lineEditNeo4JServerBolt.text())
        self.config.setNeo4jServerHttp(self.lineEditNeo4JServerHttp.text())
        self.config.setNeo4jUsername(self.lineEditNeo4JUser.text())
        self.config.setNeo4jPassword(self.lineEditNeo4JPassword.text())
        self.config.setNLPServerHttp(self.lineEditNLPServer.text())
        self.config.setBrowserPath(self.lineEditBrowserPath.text())

        self.printConfig()

        self.showConfig()

    def restoreDefaults(self):
        self.config.restoreDefaults()

        self.printConfig()

        self.showConfig()

    def chooseBrowser(self):
        root = tk.Tk()
        root.withdraw()
        browserPath = filedialog.askopenfilename(parent = root, title = 'Sélectionnez un navigateur')

        if not browserPath:
            pass
        else:
            self.lineEditBrowserPath.setText(browserPath + " %s")

    def printConfig(self):
        print(self.config.getNeo4jServerBolt())
        print(self.config.getNeo4jServerHttp())
        print(self.config.getNeo4jUsername())
        print(self.config.getNeo4jPassword())
        print(self.config.getNLPServerHttp())
        print(self.config.getBrowserPath())

if __name__ == '__main__':
    app = QApplication(sys.argv)
    ex = SettingsGUI()
    sys.exit(app.exec_())