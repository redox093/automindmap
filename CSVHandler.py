# -*- coding: utf-8 -*-
import csv, os, ntpath
from Config import *

class CSVHandler:
    def __init__(self):
        self.config = Config()
 
    def clearCSV(self, csvFilePath):    
        # Clear csv file
        print("\nClearing csv dump file")
        with open(csvFilePath,"w+") as f:
            f.close()
           
    def writeToDump(self, output, csvFilePath):
        # Looping over each sentence retrieved from Stanford CoreNLP Pipeline
        print("\nWriting data to csv dump file\n")
        for idx in output["sentences"]:
            result = [idx["openie"] for item in output] 
            # print(result) 
            for i in result: 
                for rel in i: 
                    relationSent = rel['subject'],rel['relation'],rel['object'] 
                    print(relationSent) 
                    
                    # Sending each triple to text file, in CSV format (for now)
                    if os.stat(csvFilePath).st_size == 0:
                        with open(csvFilePath,"a") as f:
                            temp = 'subject','relation','object'
                            f.write(','.join(temp))
                            f.write(''.join('\n'))
                    try:
                        with open(csvFilePath,"a") as f:
                            f.write(','.join(relationSent))
                            f.write(''.join('\n'))
                    except Exception as e:
                        print(str(e))

        # self.deleteExtraColumn()

    def appendCSVtoDump(self, content, csvFilePath):
        with open(csvFilePath,"a") as f:
            f.write(content)

    def writeCSV(self, output, csvFilePath):
        # Getting the right file name
        fileName = ntpath.basename(csvFilePath) + ".csv"

        # Create file
        with open("csvFolder/" + fileName, "w+") as f:
            f.close()

        # Fill it up
        for idx in output["sentences"]:
            result = [idx["openie"] for item in output] 
            for i in result: 
                for rel in i: 
                    relationSent = rel['subject'],rel['relation'],rel['object'] 
                    print(relationSent) 
                    
                    # Sending each triple to text file, in CSV format (for now)
                    if os.stat("csvFolder/" + fileName).st_size == 0:
                        with open("csvFolder/" + fileName,"a") as f:
                            temp = 'subject','relation','object'
                            f.write(','.join(temp))
                            f.write(''.join('\n'))
                    try:
                        with open("csvFolder/" + fileName,"a") as f:
                            f.write(','.join(relationSent))
                            f.write(''.join('\n'))
                    except Exception as e:
                        print(str(e))

    def deleteExtraColumn(self):
        print("hellahoe")
        with open(self.config.getcsvDumpPath(), "r") as source:
            rdr = csv.reader(source)
            print("hollahoe")

            with open(self.config.getcsvDumpPath(), "a") as result:
                wtr = csv.writer(result)
                for r in rdr:
                    wtr.writerow((r[0], r[1], r[2]))
                    print("holla")
