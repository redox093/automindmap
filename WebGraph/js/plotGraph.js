

function plotGraph(subject = 'n', relationship = 'l', object = 'm') {

    const elem = document.getElementById('3d-graph');
    const driver = neo4j.v1.driver("bolt://localhost:7687", neo4j.v1.auth.basic("neo4j", "s0mep4ssw0rd"));
    const session = driver.session();
    const start = new Date()
    
    let query = 'MATCH (' + subject + ')-[' + relationship + ']->(' + object + ') RETURN { id: id(n), label:head(labels(n)), caption:n.Name } as source, { id: id(l), caption:l.Rel } as relation, { id: id(m), label:head(labels(m)), caption:m.Name } as target LIMIT 100000';
        
    session
        .run(query, {limit: 5000})
        .then(function (result) {
            const nodes = {}
            const links = result.records.map(r => { 
                var source = r.get('source'); source.id = source.id.toNumber(); nodes[source.id] = source;
                var rel = r.get('relation');
                var target = r.get('target'); target.id = target.id.toNumber(); nodes[target.id] = target;
                return {source:source.id, rel, target:target.id}
            });
            
            session.close();

            console.log(links.length+" links loaded in "+(new Date()-start)+" ms.");

            const gData = { nodes: Object.values(nodes), links: links };
            const Graph = ForceGraph3D({ controlType: 'fly' })(elem)

            .graphData(gData)
            .linkDirectionalArrowLength(5)
            .linkDirectionalArrowRelPos(1)
            .nodeAutoColorBy('label')
            .nodeLabel(node => `${node.label}: ${node.caption}`)
            .nodeThreeObject(node => {
                // use a sphere as a drag handle
                const obj = new THREE.Mesh(
                    new THREE.SphereGeometry(10),
                    new THREE.MeshBasicMaterial({ depthWrite: false, transparent: true, opacity: 0 })
                );
                // add text sprite as child
                const sprite = new SpriteText(node.caption);
                sprite.color = node.color;
                sprite.textHeight = 8;
                obj.add(sprite);
                return obj;
            })
            .linkThreeObject(link => {
                // create line object
                const lineObj = new THREE.Line(
                new THREE.BufferGeometry(),
                new THREE.MeshLambertMaterial({
                    color: '#f0f0f0',
                    transparent: true,
                    opacity: 0.2
                    })
                );
                // add text sprite as child
                const sprite = new SpriteText(`${link.rel.caption}`);
                sprite.color = 'lightgrey';
                sprite.textHeight = 3;
                lineObj.add(sprite);
                return lineObj;
            })
            .linkPositionUpdate((obj, { start, end }) => {
                const middlePos = Object.assign(...['x', 'y', 'z'].map(c => ({
                [c]: start[c] + (end[c] - start[c]) / 2 // calc middle point
                })));
                const textSprite = obj.children[0];
                // Position link text
                Object.assign(textSprite.position, middlePos);
            })
            //.d3Force("link", d3.forceLink().distance(d => d.distance))
            .onNodeHover(node => elem.style.cursor = node ? 'pointer' : null)
            .onNodeClick(node => {
                // Aim at node from outside it
                const distance = 40;
                const distRatio = 1 + distance/Math.hypot(node.x, node.y, node.z);
                Graph.cameraPosition(
                    { x: node.x * distRatio, y: node.y * distRatio, z: node.z * distRatio }, // new position
                    node, // lookAt ({ x, y, z })
                    3000  // ms transition duration
                );
            });
        })
        .catch(function (error) {
            console.log(error);
    });
}

function searchSubject() {
    if ($('#searchWord').val() != undefined) {
        plotGraph("n { Name: '" + $('#searchWord').val() + "' }");
    }
    else console.log("Seems like this was undefined my friend")
}

function searchObject() {
    if ($('#searchWord').val() != undefined) {
        plotGraph("n", "l", "m { Name: '" + $('#searchWord').val() + "' }");
    }
    else console.log("Seems like this was undefined my friend")
}

function searchRelationship() {
    if ($('#searchWord').val() != undefined) {
        plotGraph("n", "l { Rel: '" + $('#searchWord').val() + "' }");
    }
    else console.log("Seems like this was undefined my friend")
}