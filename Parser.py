# -*- coding: utf-8 -*-
from tika import parser
import tkinter as tk
from tkinter import filedialog

class Parser:    
    def __init__(self):
        pass

    def loadMultipleFiles(self):
        root = tk.Tk()
        root.withdraw()
        fileList = root.tk.splitlist(filedialog.askopenfilenames(parent = root, title = 'Choisissez un ou plusieurs fichiers'))

        return fileList
    
    def parseFile(self, filepath):
        try:
            # Input data from any file format (PDF, DOC, DOCX, ODT, TXT) from local or web URL
            # Implement multiple inputs/batch processing
            # Extend to Video, Audio, Image and other exotic input source once project is more advanced : See list of supported formats and according tools http://tika.apache.org/1.10/formats.html
            # Try to write directory files names to csv, then iterate of each to parse content
            
            raw = parser.from_file(filepath)
            s = raw['content']
        except Exception as e:
            # Try to open as a single file
            s = "Something has gone wrong."
            
        return s