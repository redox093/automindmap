# -*- coding: utf-8 -*-
from PyQt5.QtWidgets import *
from PyQt5.QtGui import *
from PyQt5.QtCore import *
import sys
import ctypes
import subprocess
import nltk
from pycorenlp import *
from py2neo import *
from CSVHandler import *
from Parser import *
from WebGraph import *
from DBHandler import *
from NLPPipeline import *
from SettingsGUI import *
from Config import *
import tkinter as tk
from tkinter import filedialog
import webbrowser
import socket
from urllib import *
import shutil
import os
from Worker import *
import threading
import mimetypes
import ntpath
import io

class App(QMainWindow):

    def __init__(self, *args, **kwargs):
        super(App, self).__init__(*args, **kwargs)
        self.app = QApplication(sys.argv)

        self.title = 'Parse 2 Graph - Main Panel'
        self.left = 100
        self.top = 100
        self.width = 1080
        self.height = 720

        self.config = Config()

        # Start CoreNLP server command
        self.javaCmd = 'java -mx4g -cp "*" edu.stanford.nlp.pipeline.StanfordCoreNLPServer -port 9000 -timeout 15000'
        
        self.csvFilePath = ""
        self.textFilePath = ""
        self.textFilePaths = []
        self.text = ""

        self.parser = Parser()
        self.csvhandler = CSVHandler()
        self.webgraph = WebGraph()
        self.dbhandler = DBHandler()
        self.nlppipeline = NLPPipeline()

        self.startCoreNLP()
        # self.startNeo4J()

        self.clearCSVDirectory()
        self.clearTextFilesDirectory()

        self.initUI()

        self.advanced = False

        self.threadpool = QThreadPool()

        stylesheet = """
        
        QTableView#nlpTable { 
            background-color:rgb(190,1,1); 
            border-radius:20px;
        }
        
        QTableView#neoTable { 
            background-color:rgb(190,1,1); 
            border-radius:20px;
        }
        
        """

        self.app.setStyleSheet(stylesheet)

        self.startIndicatorsThread()

        sys.exit(self.app.exec_())
    
    def initUI(self):

        # Window title, icon and geometry
        self.setWindowTitle(self.title)
        self.setWindowIcon(QIcon('images/mindmap3.png'))
        self.setGeometry(self.left, self.top, self.width, self.height)
        
        # Windows taskbar icon
        self.appid = 'parse2graphicon'
        ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(self.appid)

        # Main Menu
        self.mainMenu = self.menuBar()
        self.fileMenu = self.mainMenu.addMenu('Fichier')
        self.optionsMenu = self.mainMenu.addMenu('Options')
        self.toolsMenu = self.mainMenu.addMenu('Outils')
        self.helpMenu = self.mainMenu.addMenu('Aide')

        # Sub Menus
        self.loadCSVButton = QAction(QIcon('images/csv1.png'), 'Sélectionner un fichier CSV', self)
        self.loadCSVButton.setShortcut('Ctrl+U')
        self.loadCSVButton.setStatusTip("Sélectionner un fichier CSV")
        self.loadCSVButton.triggered.connect(self.loadCSVFile)
        self.fileMenu.addAction(self.loadCSVButton)

        self.saveCSVButton = QAction(QIcon('images/csvdown.png'), 'Enregistrer le fichier CSV', self)
        self.saveCSVButton.setShortcut('Ctrl+D')
        self.saveCSVButton.setStatusTip("Enregistrer le fichier CSV")
        self.saveCSVButton.triggered.connect(self.saveCSVFile)
        self.fileMenu.addAction(self.saveCSVButton)

        self.saveCSVBundleButton = QAction(QIcon('images/csvdown.png'), 'Enregistrer les fichiers CSV', self)
        self.saveCSVBundleButton.setShortcut('Ctrl+M')
        self.saveCSVBundleButton.setStatusTip("Enregistrer les fichiers CSV")
        self.saveCSVBundleButton.triggered.connect(self.saveCSVBundle)
        self.fileMenu.addAction(self.saveCSVBundleButton)

        self.saveTXTBundleButton = QAction(QIcon('images/text.png'), 'Enregistrer les fichiers text', self)
        self.saveTXTBundleButton.setShortcut('Ctrl+T')
        self.saveTXTBundleButton.setStatusTip("Enregistrer les fichiers TXT")
        self.saveTXTBundleButton.triggered.connect(self.saveTextBundle)
        self.fileMenu.addAction(self.saveTXTBundleButton)

        self.exitButton = QAction(QIcon('images/exit.png'), 'Quitter', self)
        self.exitButton.setShortcut('Ctrl+Q')
        self.exitButton.setStatusTip("Quitter l'application")
        self.exitButton.triggered.connect(self.close)
        self.fileMenu.addAction(self.exitButton)

        self.simpleButton = QAction(QIcon('images/one.png'), 'Show simple panel', self)
        self.simpleButton.setShortcut('Ctrl+S')
        self.simpleButton.setStatusTip("Afficher l'interface simple")
        self.simpleButton.triggered.connect(self.hideAdvancedPanel)
        self.optionsMenu.addAction(self.simpleButton)

        self.advancedButton = QAction(QIcon('images/three.png'), 'Toggle advanced panel', self)
        self.advancedButton.setShortcut('Ctrl+A')
        self.advancedButton.setStatusTip("Afficher l'interface avancée")
        self.advancedButton.triggered.connect(self.showAdvancedPanel)
        self.optionsMenu.addAction(self.advancedButton)

        self.settingsButton = QAction(QIcon('images/settings.png'), 'Paramètres', self)
        self.settingsButton.setShortcut('Ctrl+P')
        self.settingsButton.setStatusTip('Ouvrir la fenêtre des paramètres')
        self.settingsButton.triggered.connect(self.openSettingsWindow)
        self.toolsMenu.addAction(self.settingsButton)

        self.clearCSVDirButton = QAction(QIcon('images/clear.png'), 'Vider le dossier CSV', self)
        self.clearCSVDirButton.setStatusTip('Vider le dossier CSV')
        self.clearCSVDirButton.triggered.connect(self.clearCSVDirectory)
        self.toolsMenu.addAction(self.clearCSVDirButton)

        self.clearTXTDirButton = QAction(QIcon('images/clear2.png'), 'Vider le dossier TXT', self)
        self.clearTXTDirButton.setStatusTip('Vider le dossier TXT')
        self.clearTXTDirButton.triggered.connect(self.clearTextFilesDirectory)
        self.toolsMenu.addAction(self.clearTXTDirButton)

        self.clearBDButton = QAction(QIcon('images/neo4j.png'), 'Vider la base de données', self)
        self.clearBDButton.setStatusTip('Vider la base de données')
        self.clearBDButton.triggered.connect(self.dbhandler.emptyDB)
        self.toolsMenu.addAction(self.clearBDButton)

        self.readmeButton = QAction(QIcon('images/readme.png'), 'Lisez-Moi', self)
        self.readmeButton.setShortcut('Ctrl+H')
        self.readmeButton.setStatusTip("Lire le fichier Lisez-Moi")
        self.readmeButton.triggered.connect(self.openReadMe)
        self.helpMenu.addAction(self.readmeButton)

        # Labels
        self.labelStatus = QLabel(self)
        self.labelStatus.setGeometry(QRect(100, 40, 140, 40))
        font = QFont()
        font.setPointSize(14)
        self.labelStatus.setFont(font)
        self.labelStatus.setAlignment(Qt.AlignCenter)
        self.labelStatus.setObjectName("labelStatus")
        self.labelStatus.setText("État")

        self.labelOperations = QLabel(self)
        self.labelOperations.setGeometry(QRect(690, 40, 140, 40))
        font = QFont()
        font.setPointSize(14)
        self.labelOperations.setFont(font)
        self.labelOperations.setAlignment(Qt.AlignCenter)
        self.labelOperations.setObjectName("labelOperations")
        self.labelOperations.setText("Opérations")

        self.labelData = QLabel(self)
        self.labelData.setGeometry(QRect(1320, 40, 160, 40))
        font = QFont()
        font.setPointSize(14)
        self.labelData.setFont(font)
        self.labelData.setAlignment(Qt.AlignCenter)
        self.labelData.setObjectName("labelData")
        self.labelData.setText("Données brutes")

        self.labelNLPStatus = QLabel(self)
        self.labelNLPStatus.setGeometry(QRect(300, 35, 210, 15))
        self.labelNLPStatus.setObjectName("labelNLPStatus")
        self.labelNLPStatus.setText("Serveur CoreNLP : ")

        self.labelNeo4jStatus = QLabel(self)
        self.labelNeo4jStatus.setGeometry(QRect(300, 65, 210, 15))
        self.labelNeo4jStatus.setObjectName("labelNeo4jStatus")
        self.labelNeo4jStatus.setText("Serveur Neo4j : ")

        self.labelFiles = QLabel(self)
        self.labelFiles.setGeometry(QRect(700, 700, 210, 15))
        self.labelFiles.setObjectName("labelFiles")
        self.labelFiles.setText("Fichier(s) en traitement : ")

        self.labelFilesNames = QLabel(self)
        self.labelFilesNames.setGeometry(QRect(675, 725, 300, 300))
        self.labelFilesNames.setObjectName("labelFilesNames")
        self.labelFilesNames.setAlignment(Qt.AlignAbsolute)


        # Vertical Layout & Content
        self.verticalLayoutWidget = QWidget(self)
        self.verticalLayoutWidget.setGeometry(QRect(660, 100, 210, 150))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")

        self.verticalLayout = QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")

        self.parseTextBtn = QPushButton(self.verticalLayoutWidget)
        self.parseTextBtn.setObjectName("parseTextBtn")
        self.parseTextBtn.setText("Extraire le texte")

        self.extractContentBtn = QPushButton(self.verticalLayoutWidget)
        self.extractContentBtn.setObjectName("extractContentBtn")
        self.extractContentBtn.setText("Extraire les données")

        self.writeToDBBtn = QPushButton(self.verticalLayoutWidget)
        self.writeToDBBtn.setObjectName("writeToDBBtn")
        self.writeToDBBtn.setText("Écrire la base de données")

        self.verticalLayout.addWidget(self.parseTextBtn)
        self.verticalLayout.addWidget(self.extractContentBtn)
        self.verticalLayout.addWidget(self.writeToDBBtn)

        # Horizontal Layout & Content
        self.horizontalLayoutWidget = QWidget(self)
        self.horizontalLayoutWidget.setGeometry(QRect(740, 50, 40, 610))
        self.horizontalLayoutWidget.setObjectName("horizontalLayoutWidget")
        self.horizontalLayout = QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")

        spacerItem = QSpacerItem(20, 568, QSizePolicy.Minimum, QSizePolicy.Expanding)
        self.horizontalLayout.addItem(spacerItem)

        # Buttons
        self.chooseDocBtn = QPushButton(self)
        self.chooseDocBtn.setGeometry(QRect(150, 820, 230, 50))
        self.chooseDocBtn.setObjectName("chooseDocBtn")
        self.chooseDocBtn.setText("Choisir un document")

        self.textToVizBtn = QPushButton(self)
        self.textToVizBtn.setGeometry(QRect(150, 880, 230, 50))
        self.textToVizBtn.setObjectName('textToVizBtn')
        self.textToVizBtn.setText('Visualiser le réseau de concepts')

        self.textToCSVBtn = QPushButton(self)
        self.textToCSVBtn.setGeometry(QRect(680, 270, 170, 50))
        self.textToCSVBtn.setObjectName("textToCSVBtn")
        self.textToCSVBtn.setText("Texte -> CSV")

        self.textToDBBtn = QPushButton(self)
        self.textToDBBtn.setGeometry(QRect(680, 330, 170, 50))
        self.textToDBBtn.setObjectName("textToDBBtn")
        self.textToDBBtn.setText("Texte -> Database")

        self.csvToDBBtn = QPushButton(self)
        self.csvToDBBtn.setGeometry(QRect(680, 435, 170, 50))
        self.csvToDBBtn.setObjectName("csvToDBBtn")
        self.csvToDBBtn.setText("CSV -> Database")

        self.openVizBtn = QPushButton(self)
        self.openVizBtn.setGeometry(QRect(680, 600, 170, 50))
        self.openVizBtn.setObjectName("openVizBtn")
        self.openVizBtn.setText("Ouvrir la visualisation")

        self.openDBBtn = QPushButton(self)
        self.openDBBtn.setGeometry(QRect(680, 540, 170, 50))
        self.openDBBtn.setObjectName("openDBBtn")
        self.openDBBtn.setText("Ouvrir la base de données")

        self.clearTableBtn = QPushButton(self)
        self.clearTableBtn.setGeometry(QRect(1320, 920, 170, 50))
        self.clearTableBtn.setObjectName("clearTableBtn")
        self.clearTableBtn.setText("Vider le tableau")

        # Console / Terminal / Message Box
        self.statusConsole = QPlainTextEdit(self)
        self.statusConsole.setGeometry(QRect(90, 100, 350, 700))
        self.statusConsole.setReadOnly(True)
        self.statusConsole.setObjectName("statusConsole")

        # Table
        self.model = QStandardItemModel(self)
        self.csvTable = QTableView(self)
        self.csvTable.setModel(self.model)
        self.csvTable.setGeometry(QRect(1100, 100, 600, 800))
        self.csvTable.horizontalHeader().setSectionResizeMode(QHeaderView.Stretch)
        self.csvTable.setObjectName("csvTable")

        self.modelNLP = QStandardItemModel(self)
        self.nlpTable = QTableView(self)
        self.nlpTable.setModel(self.modelNLP)
        self.nlpTable.setGeometry(400, 35, 20, 20)
        self.nlpTable.setObjectName("nlpTable")

        self.modelNeo4j = QStandardItemModel(self)
        self.neoTable = QTableView(self)
        self.neoTable.setModel(self.modelNeo4j)
        self.neoTable.setGeometry(400, 65, 20, 20)
        self.neoTable.setObjectName("neoTable")

        # Progress Bar
        self.progressBarAdvanced = QProgressBar(self)
        self.progressBarAdvanced.setGeometry(600, 920, 400, 50)
        self.progressBarAdvanced.hide()

        self.progressBarSimple = QProgressBar(self)
        self.progressBarSimple.setGeometry(90, 950, 400, 30)
        self.progressBarSimple.hide()

        # Timer
        self.timer = QTimer()
        self.timer.setInterval(5000)
        self.timer.timeout.connect(self.startIndicatorsThread)
        self.timer.start()

        # Buttons clicked bindings
        self.chooseDocBtn.clicked.connect(self.selectMultipleFiles)
        self.parseTextBtn.clicked.connect(lambda: self.parseFile(self.textFilePath))
        self.extractContentBtn.clicked.connect(self.extractContent)
        self.writeToDBBtn.clicked.connect(self.writeDatabase)
        self.textToDBBtn.clicked.connect(self.textToDB)
        self.csvToDBBtn.clicked.connect(self.csvToDatabase)
        self.openVizBtn.clicked.connect(self.openWebGraph)
        self.openDBBtn.clicked.connect(self.openNeo4JBrowser)
        self.textToVizBtn.clicked.connect(self.startTextToVizThread)
        #self.textToCSVBtn.clicked.connect(self.saveCSVBundle)
        self.clearTableBtn.clicked.connect(self.clearCsvTable)

        self.hideAdvancedPanel()

        self.show()

    def vizProgressFn(self, n):
        progress = n * 100 / len(self.textFilePaths)
        self.progressBarSimple.setValue(progress)
        self.progressBarAdvanced.setValue(progress)
        try:
            self.statusConsole.appendPlainText("Traitement du fichier : " + self.textFilePaths[n] + " \n")
        except IndexError:
            pass
        print("%d%% done" % progress)
        if progress == 100:
            self.progressBarSimple.hide()
            self.progressBarAdvanced.hide()

    def indicatorsProgressFn(self, n):
        if n == 1:
            self.styleThatSheet(True, True)
        elif n == 2:
            self.styleThatSheet(False, True)
        elif n == 3:
            self.styleThatSheet(True, False)
        elif n == 4:
            self.styleThatSheet(False, False)
        
    def print_output(self, s):
        print(s)
        
    def indicatorsThreadComplete(self):
        print("Indicators Thread completed !")

    def vizThreadComplete(self):
        self.writeDatabase()
        self.loadCsvToTable()
        self.openWebGraph()
        print("Viz Thread completed !")

    def startIndicatorsThread(self):
        
        # Pass the function to execute
        worker = Worker(self.setIndicators) # Any other args, kwargs are passed to the run function
        worker.signals.result.connect(self.print_output)
        worker.signals.finished.connect(self.indicatorsThreadComplete)
        worker.signals.progress.connect(self.indicatorsProgressFn)
        
        # Execute
        self.threadpool.start(worker) 

    def startTextToVizThread(self):

        if self.advanced:
            self.progressBarAdvanced.show()
        else:
            self.progressBarSimple.show()

        worker2 = Worker(self.textToViz)
        worker2.signals.result.connect(self.print_output)
        worker2.signals.finished.connect(self.vizThreadComplete)
        worker2.signals.progress.connect(self.vizProgressFn)

        self.threadpool.start(worker2) 

    def textToViz(self, progress_callback):
        self.textToDB(progress_callback)


    def clearCsvTable(self):
        self.model.clear()

    def loadCsvToTable(self):
        self.clearCsvTable()
        with open(self.config.getcsvDumpPath(), "r") as fileInput:
            for row in csv.reader(fileInput):    
                items = [QStandardItem(field) for field in row]
                self.model.appendRow(items)

    def styleThatSheet(self, nlp = False, neo = False):
        if neo and nlp:
            stylesheet = """
                QTableView#nlpTable { 
                    background-color:rgb(50,205,50); 
                    border-radius:14px;
                }

                QTableView#neoTable { 
                    background-color:rgb(50,205,50); 
                    border-radius:14px;
                }

                """
        elif neo and not nlp:
            stylesheet = """
                QTableView#nlpTable { 
                    background-color:rgb(190,1,1); 
                    border-radius:14px;
                }

                QTableView#neoTable { 
                    background-color:rgb(50,205,50); 
                    border-radius:14px;
                }

                """
        elif nlp and not neo:
            stylesheet = """
                QTableView#nlpTable { 
                    background-color:rgb(50,205,50); 
                    border-radius:14px;
                }

                QTableView#neoTable { 
                    background-color:rgb(190,1,1); 
                    border-radius:14px;
                }

                """
        elif not nlp and not neo:
            stylesheet = """
                QTableView#nlpTable { 
                    background-color:rgb(190,1,1); 
                    border-radius:14px;
                }

                QTableView#neoTable { 
                    background-color:rgb(190,1,1); 
                    border-radius:14px;
                }

                """
        self.app.setStyleSheet(stylesheet)

    # App startup

    def startCoreNLP(self):
        try:
            subprocess.Popen(self.javaCmd, shell=True, cwd='./CoreNLP')
        except Exception as e:
            self.plainTextEdit.appendPlainText("Un problème est survenu au démarrage du moteur d'analyse du langage... Avez-vous installé Java sur votre PATH ? \n")
            self.plainTextEdit.appendPlainText("Voici plus de détails : " + str(e) + " \n")

    def startNeo4J(self):
        try:
            subprocess.Popen("Neo4j.exe", shell=True, cwd='C:/Program Files/Neo4j Desktop')
        except Exception as e:
            self.plainTextEdit.appendPlainText("Something went wrong while starting the natural language processing engine... do you have java installed on your path ? \n")
            self.plainTextEdit.appendPlainText("Here are more details : " + str(e) + " \n")

    # Recurring functions

    ##### Check if CoreNLP and Neo4j servers are online
    def setIndicators(self, progress_callback):
        try:
            if request.urlopen(self.config.getNeo4jServerHttp()).getcode() == 200:
                try:
                    if request.urlopen(self.config.getNLPServerHttp()).getcode() == 200:
                        progress_callback.emit(1)
                except Exception as e1:
                    progress_callback.emit(2)
                    print("NLP server is down" + str(e1))
        except Exception as e:
            try:
                if request.urlopen(self.config.getNLPServerHttp()).getcode() == 200:
                    progress_callback.emit(3)
                    print("NEO server is down " + str(e))
            except Exception as e2:
                progress_callback.emit(4)
                print("NLP + NEO both servers are down" + str(e2))

    # File menu functions

    def saveCSVFile(self):
        root = tk.Tk()
        root.withdraw()
        saveFile = filedialog.asksaveasfile(mode='w', defaultextension=".csv")

        if saveFile is None:
            self.statusConsole.appendPlainText("Vous n'avez pas entré de nom de fichier.  L'opération ne sera pas exécutée. \n")
            return

        with open(self.config.getcsvDumpPath(), 'r') as csvDump:
            data = csvDump.read()

        saveFile.write(data)
        saveFile.close()
        self.statusConsole.appendPlainText("Le fichier CSV a bien été copié dans le dossier sélectionné \n")

    def loadCSVFile(self, csvFilePath = None):
        root = tk.Tk()
        root.withdraw()
        self.csvFilePath = filedialog.askopenfilename()

        if not self.csvFilePath:
            self.statusConsole.appendPlainText("Aucun fichier CSV n'a été sélectionné \n")
            return
        else:
            with open(self.csvFilePath, 'r') as csvFile:
                data = csvFile.read()
            with open(self.config.getcsvDumpPath(), 'w') as csvDump:
                csvDump.write(data)
                csvDump.close()
            self.loadCsvToTable()
            self.statusConsole.appendPlainText("Vous avez sélectionné le fichier CSV suivant : " + self.csvFilePath +" \n")

    def loadMultipleCSVFiles(self):
        root = tk.Tk()
        root.withdraw()
        fileList = root.tk.splitlist(filedialog.askopenfilenames(parent = root, title = 'Choisissez un ou plusieurs fichiers'))

    def saveCSVBundle(self):
        root = tk.Tk()
        root.withdraw()
        self.copytree(os.path.abspath(self.config.getcsvFolderPath()), filedialog.askdirectory())
        self.statusConsole.appendPlainText("Les fichiers CSV ont bien été copiés dans le dossier sélectionné \n")
    
    def saveTextBundle(self):
        root = tk.Tk()
        root.withdraw()
        self.copytree(os.path.abspath(self.config.getTextFolderPath()), filedialog.askdirectory())
        self.statusConsole.appendPlainText("Les fichiers TXT ont bien été copiés dans le dossier sélectionné \n")

    # Options menu functions

    def showAdvancedPanel(self):
        self.title = 'Parse 2 Graph - Main Panel - Advanced'
        self.left = 50
        self.top = 50
        self.width = 1800
        self.height = 1000
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.setFixedSize(self.width, self.height)

        self.parseTextBtn.show()
        self.extractContentBtn.show()
        self.writeToDBBtn.show()
        self.textToDBBtn.show()
        self.openDBBtn.show()
        self.openVizBtn.show()

        self.advanced = True

        self.show()

    def hideAdvancedPanel(self):
        self.title = 'Parse 2 Graph - Main Panel'
        self.left = 50
        self.top = 50
        self.width = 530
        self.height = 1000
        self.setGeometry(self.left, self.top, self.width, self.height)
        self.setFixedSize(self.width, self.height)

        self.parseTextBtn.hide()
        self.extractContentBtn.hide()
        self.writeToDBBtn.hide()
        self.textToDBBtn.hide()
        self.openDBBtn.hide()
        self.openVizBtn.hide()

        self.advanced = False

        self.show()

    # Tools menu functions 

    def openSettingsWindow(self):
        self.settings = SettingsGUI()

    def clearCSVDirectory(self):
        folder = self.config.getcsvFolderPath()

        for f in os.listdir(folder):
            path = os.path.join(folder, f)
            try:
                if os.path.isfile(path):
                    os.unlink(path)
            except Exception as e:
                print(e)

    def clearTextFilesDirectory(self):
        folder = self.config.getTextFolderPath()

        for f in os.listdir(folder):
            path = os.path.join(folder, f)
            try:
                if os.path.isfile(path):
                    os.unlink(path)
            except Exception as e:
                print(e)

    # Help menu functions

    def openReadMe(self):
        webbrowser.open('readme.txt')

    # Basic panel functions

    ##### Select the file to process
    def selectMultipleFiles(self):
        for f in self.textFilePaths:
            print(f)
        self.textFilePaths = self.parser.loadMultipleFiles()
        for f in self.textFilePaths:
            print(f)
        if not self.textFilePaths:
            self.statusConsole.appendPlainText("Aucun fichier n'a été sélectionné \n")
        else:
            self.labelFilesNames.setText("")
            self.statusConsole.appendPlainText("Vous avez choisi le ou les fichiers suivant : \n") 
            for f in self.textFilePaths:
                self.statusConsole.appendPlainText(f)
                self.labelFilesNames.setText(self.labelFilesNames.text() + "\n" + ntpath.basename(f))
            self.statusConsole.appendPlainText(" \n") 
            if len(self.textFilePaths) == 1:
                self.textFilePath = self.textFilePaths[0]

        self.definePossibleOperations()

    ##### Execute all operations on all text files and load resulting graph visualization

    def textToDB(self, progress_callback):
        self.extractContentAllFiles(progress_callback)
        

    # Advanced panel functions

    # Parse the file
    def parseFile(self, textFilePath):
        if not textFilePath:
            #self.statusConsole.appendPlainText("Svp choisir un fichier d'abord ! \n")
            pass
        else :
            self.text = self.parser.parseFile(textFilePath)
            self.writeTextFileToFolder(ntpath.basename(textFilePath) + ".txt", self.text.encode("utf-8"))
            #self.statusConsole.appendPlainText("Le texte de votre fichier a été extrait avec succès ! \n")

    def writeTextFileToFolder(self, fileName, text):
        # Create file
        with open(self.config.getTextFolderPath() + fileName, "w+") as f:
            f.close()

        try:
            with open(self.config.getTextFolderPath() + fileName, "wb") as f:
                f.write(text)
        except Exception as e:
            print(str(e))

    def extractContent(self):
        try:
            self.csvhandler.clearCSV(self.config.getcsvDumpPath())
            self.csvhandler.writeToDump(self.nlppipeline.extractContent(self.text), self.config.getcsvDumpPath())
            self.statusConsole.appendPlainText("L'ontologie de votre texte a été extraite ! \n")
            self.loadCsvToTable()
        except Exception as e:
            print(str(e))
            self.statusConsole.appendPlainText("L'extraction n'a pas fonctionné... avez-vous entré un URL valide ? \n")

    def extractContentAllFiles(self, progress_callback):
        self.csvhandler.clearCSV(self.config.getcsvDumpPath())
        nbProcessed = 0
        progress_callback.emit(nbProcessed)
        for f in self.textFilePaths:
            mimeType, _ = mimetypes.guess_type(f)
            if mimeType == "text/plain":
                print("not parsing bitch")
                textFile = open(f, "r")
                self.text = textFile.read()
            elif mimeType == "application/pdf" \
                or mimeType == "application/vnd.openxmlformats-officedocument.wordprocessingml.document" \
                or mimeType == "application/msword" \
                or mimeType == "application/vnd.oasis.opendocument.text":
                self.parseFile(f)
            elif mimeType == "application/vnd.ms-excel" or "text/csv":
                self.text = ""
                with open(f, "r") as csvContent:
                    if os.stat(self.config.getcsvDumpPath()).st_size == 0:
                        self.csvhandler.appendCSVtoDump(csvContent.read(), self.config.getcsvDumpPath())
                    else:
                        next(csvContent)
                        self.csvhandler.appendCSVtoDump(csvContent.read(), self.config.getcsvDumpPath())
            else: pass
            try:
                output = self.nlppipeline.extractContent(self.text)
                self.csvhandler.writeToDump(output, self.config.getcsvDumpPath())
                self.csvhandler.writeCSV(output, f)
                #self.statusConsole.appendPlainText("L'ontologie de votre texte a été extraite ! \n")
            except Exception as e:
                print(str(e))
                #self.statusConsole.appendPlainText("L'extraction n'a pas fonctionné... avez-vous entré un URL valide ? \n")
            nbProcessed += 1
            progress_callback.emit(nbProcessed)
        #self.loadCsvToTable()

    def writeDatabase(self):
        try:
            self.dbhandler.emptyDB()
            self.dbhandler.populateDB()
            self.statusConsole.appendPlainText("Les concepts et relations ont été transmis a la base de donnée avec succès ! \n")
        except Exception as e:
            print(str(e))
            self.statusConsole.appendPlainText("La base de données est inaccessible... avez-vous entré la bonne addresse ? Le serveur est-il opérationnel ? \n")

    def openWebGraph(self):
        self.statusConsole.appendPlainText("Ouverture de la visualisation... \n")
        self.webgraph.openWebGraph(self.config.getBrowserPath(), self.config.getWebGraphPath())

    def openNeo4JBrowser(self):
        self.statusConsole.appendPlainText("Vous aurez peut-être besoin du mot de passe suivant pour accéder a la base de données : " + self.config.getNeo4jPassword() + "\n")
        self.webgraph.openNeo4JBrowser(self.config.getBrowserPath(), self.config.getNeo4jBrowserPath())
        self.statusConsole.appendPlainText("Entrez la commande suivante pour afficher tous les concepts et relations contenus dans le graphe : MATCH (n)-[r]->(m) RETURN n,r,m; \n")


    def definePossibleOperations(self):
        if len(self.textFilePaths) <= 1:
            mimeType, _ = mimetypes.guess_type(self.textFilePath)
            if mimeType == "text/plain":
                self.setBtnsForTextFiles()
            elif mimeType == "application/vnd.ms-excel" or "text/csv":
                self.setBtnsForCsvFiles()
            elif mimeType == "application/pdf" \
                or mimeType == "application/vnd.openxmlformats-officedocument.wordprocessingml.document" \
                or mimeType == "application/msword" \
                or mimeType == "application/vnd.oasis.opendocument.text":
                    self.setBtnsForParsedFiles()
            else:
                self.setAllBtnsEnabled()
        else:
            needsParsing = 0
            text = 0
            csv = 0
            for f in self.textFilePaths:
                mimeType, _ = mimetypes.guess_type(f)
                print(mimeType)
                if mimeType == "text/plain":
                    print("do i")
                    text += 1
                elif mimeType == "application/pdf" \
                    or mimeType == "application/vnd.openxmlformats-officedocument.wordprocessingml.document" \
                    or mimeType == "application/msword" \
                    or mimeType == "application/vnd.oasis.opendocument.text":
                    print("pdf")
                    needsParsing += 1
                elif mimeType == "application/vnd.ms-excel" or "text/csv":
                    print("how do i even")
                    csv += 1
            if needsParsing > 0 and text > 0 or csv > 0:
                print("why tho")
                print("needsParsing " + str(needsParsing))
                print("text " + str(text))
                print("csv " + str(csv))
                self.setAllBtnsDisabled()
            else: self.setAllBtnsDisabled()
                
                    
    def setAllBtnsEnabled(self):
        self.parseTextBtn.setEnabled(True)
        self.extractContentBtn.setEnabled(True)
        self.writeToDBBtn.setEnabled(True)
        self.textToCSVBtn.setEnabled(True)
        self.textToDBBtn.setEnabled(True)
        self.csvToDBBtn.setEnabled(True)

    def setAllBtnsDisabled(self):
        self.parseTextBtn.setEnabled(False)
        self.extractContentBtn.setEnabled(False)
        self.writeToDBBtn.setEnabled(False)
        self.textToCSVBtn.setEnabled(False)
        self.textToDBBtn.setEnabled(False)
        self.csvToDBBtn.setEnabled(False)

    def setBtnsForTextFiles(self):
        self.setAllBtnsEnabled()
        self.parseTextBtn.setEnabled(False)
        self.csvToDBBtn.setEnabled(False)
    
    def setBtnsForCsvFiles(self):
        self.setAllBtnsEnabled()
        self.parseTextBtn.setEnabled(False)
        self.extractContentBtn.setEnabled(False)
        self.writeToDBBtn.setEnabled(False)
        self.textToCSVBtn.setEnabled(False)
        self.textToDBBtn.setEnabled(False)

    def setBtnsForParsedFiles(self):
        self.setAllBtnsEnabled()
        self.csvToDBBtn.setEnabled(False)


    def copytree(self, src, dst, symlinks = False, ignore = None):
        for item in os.listdir(src):
            s = os.path.join(src, item)
            d = os.path.join(dst, item)
            if os.path.isdir(s):
                shutil.copytree(s, d, symlinks, ignore)
            else:
                shutil.copy2(s, d)

    def csvToDatabase(self):
        if not self.csvFilePath:
            self.statusConsole.appendPlainText("Vous devez charger un fichier CSV d'abord ! \n")
        else:
            self.dbhandler.emptyDB()
            self.dbhandler.populateDB()
            self.statusConsole.appendPlainText("Fichier CSV importé avec succès !  Cliquez sur visualiser pour voir la carte conceptuelle résultante \n")