from distutils.core import setup
import py2exe, sys, os

setup(
    options = {
        'py2exe': {
            'bundle_files': 1, 
            'compressed': True
            }
        },
    windows=[
        {
            'script': 'main.py',
            "icon_resources": [(1, "mindmap3.png")]
        }
    ],
    zipfile = None,
)