# -*- coding: utf-8 -*-
from pycorenlp import * 
import nltk
from Config import *

class NLPPipeline:
    def __init__(self):
        self.config = Config()

    def extractContent(self, textToAnalyse):
        self.nlpServer = StanfordCoreNLP(self.config.getNLPServerHttp())
        # CoreNLP API Call
        print("\nFeeding NLP engine...")
        output = self.nlpServer.annotate(textToAnalyse, properties={"annotators":"tokenize,ssplit,pos,depparse,natlog,openie", 
            "outputFormat":"json", 
            "timeout":500000,
            "openie.max_entailments_per_clause":"1",
            "openie.triple.strict":"true"
            })
                        
        return output