# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'interface.ui'
#
# Created by: PyQt5 UI code generator 5.11.3
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
import subprocess
import nltk
from pycorenlp import *
from py2neo import *
from CSVHandler import *
from Parser import *
from WebGraph import *
from DBHandler import *
from NLPPipeline import *

class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(1071, 705)
        self.pushButton = QtWidgets.QPushButton(Dialog)
        self.pushButton.setGeometry(QtCore.QRect(150, 620, 231, 51))
        self.pushButton.setObjectName("pushButton")
        self.plainTextEdit = QtWidgets.QPlainTextEdit(Dialog)
        self.plainTextEdit.setGeometry(QtCore.QRect(90, 100, 351, 491))
        self.plainTextEdit.setReadOnly(True)
        self.plainTextEdit.setObjectName("plainTextEdit")
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(190, 30, 141, 41))
        font = QtGui.QFont()
        font.setPointSize(14)
        self.label.setFont(font)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.pushButton_5 = QtWidgets.QPushButton(Dialog)
        self.pushButton_5.setGeometry(QtCore.QRect(530, 500, 171, 61))
        self.pushButton_5.setObjectName("pushButton_5")
        self.pushButton_6 = QtWidgets.QPushButton(Dialog)
        self.pushButton_6.setGeometry(QtCore.QRect(530, 420, 171, 61))
        self.pushButton_6.setObjectName("pushButton_6")
        self.verticalLayoutWidget = QtWidgets.QWidget(Dialog)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(510, 100, 211, 151))
        self.verticalLayoutWidget.setObjectName("verticalLayoutWidget")
        self.verticalLayout = QtWidgets.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.pushButton_2 = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.pushButton_2.setObjectName("pushButton_2")
        self.verticalLayout.addWidget(self.pushButton_2)
        self.pushButton_3 = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.pushButton_3.setObjectName("pushButton_3")
        self.verticalLayout.addWidget(self.pushButton_3)
        self.pushButton_4 = QtWidgets.QPushButton(self.verticalLayoutWidget)
        self.pushButton_4.setObjectName("pushButton_4")
        self.verticalLayout.addWidget(self.pushButton_4)
        self.pushButton_7 = QtWidgets.QPushButton(Dialog)
        self.pushButton_7.setGeometry(QtCore.QRect(530, 270, 171, 61))
        self.pushButton_7.setObjectName("pushButton_7")
        self.lineEdit = QtWidgets.QLineEdit(Dialog)
        self.lineEdit.setGeometry(QtCore.QRect(830, 160, 191, 51))
        self.lineEdit.setObjectName("lineEdit")
        self.label_2 = QtWidgets.QLabel(Dialog)
        self.label_2.setGeometry(QtCore.QRect(800, 100, 261, 41))
        self.label_2.setObjectName("label_2")
        self.label_3 = QtWidgets.QLabel(Dialog)
        self.label_3.setGeometry(QtCore.QRect(840, 250, 211, 16))
        self.label_3.setObjectName("label_3")
        self.lineEdit_2 = QtWidgets.QLineEdit(Dialog)
        self.lineEdit_2.setGeometry(QtCore.QRect(830, 290, 191, 51))
        self.lineEdit_2.setObjectName("lineEdit_2")
        self.pushButton_8 = QtWidgets.QPushButton(Dialog)
        self.pushButton_8.setGeometry(QtCore.QRect(800, 420, 241, 51))
        self.pushButton_8.setObjectName("pushButton_8")
        self.pushButton_9 = QtWidgets.QPushButton(Dialog)
        self.pushButton_9.setGeometry(QtCore.QRect(800, 510, 241, 51))
        self.pushButton_9.setObjectName("pushButton_9")
        self.horizontalLayoutWidget = QtWidgets.QWidget(Dialog)
        self.horizontalLayoutWidget.setGeometry(QtCore.QRect(740, 50, 41, 611))
        self.horizontalLayoutWidget.setObjectName("horizontalLayoutWidget")
        self.horizontalLayout = QtWidgets.QHBoxLayout(self.horizontalLayoutWidget)
        self.horizontalLayout.setContentsMargins(0, 0, 0, 0)
        self.horizontalLayout.setObjectName("horizontalLayout")
        spacerItem = QtWidgets.QSpacerItem(20, 568, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.horizontalLayout.addItem(spacerItem)
        


        # **Make sure that the CoreNLP server is up and running** (See README for instructions)
        self.nlpServer = StanfordCoreNLP("http://localhost:9000/")

        # Start CoreNLP server command
        self.javaCmd = 'java -mx4g -cp "*" edu.stanford.nlp.pipeline.StanfordCoreNLPServer -port 9000 -timeout 15000'
 
        # Connect to Neo4j server
        self.graph = Graph(ip_addr = 'bolt://localhost:7687', username = 'neo4j', password = 's0mep4ssw0rd')
        
        self.csvFilePath = "csvDump.csv"

        # Statement to load csv data into the neo4j database
        self.loadCSVQuery = '''LOAD CSV WITH HEADERS FROM "file:///C:/Users/Prod/Desktop/automindmap/csvDump.csv" AS line 
        MERGE (subj:Subject{Name:line.subject}) 
        MERGE (obj:Object{Name:line.object}) 
        MERGE (subj) -[:TO {Rel:line.relation}]-> (obj)'''
        
        # Query to empty the database
        self.emptyDBQuery = '''MATCH (n)-[r]->(m) DELETE n,r,m;'''
        
        # Chrome Path because webbrowser opens IE by default... -.-
        self.browserPath = 'C:/Program Files (x86)/Google/Chrome/Application/chrome.exe %s'
        
        # Browser viz paths
        self.graphPath = 'WebGraph/graph.html'
        self.Neo4JBrowserPath = 'http://localhost:7474/browser/'
        
        self.filePath = ""
        self.text = ""
        
        self.parser = Parser()
        self.csvhandler = CSVHandler()
        self.webgraph = WebGraph()
        self.dbhandler = DBHandler()
        self.nlppipeline = NLPPipeline()
        
        self.pushButton.clicked.connect(self.selectFile)
        self.pushButton_2.clicked.connect(self.parseFile)
        self.pushButton_3.clicked.connect(self.extractContent)
        self.pushButton_4.clicked.connect(self.writeDatabase)
        self.pushButton_5.clicked.connect(self.openWebGraph)
        self.pushButton_6.clicked.connect(self.openNeo4JBrowser)
        self.pushButton_7.clicked.connect(self.textToDB)

        self.startCoreNLP()
        
        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "Parse2Graph"))
        self.pushButton.setText(_translate("Dialog", "Choisir un document"))
        self.label.setText(_translate("Dialog", "Status"))
        self.pushButton_5.setText(_translate("Dialog", "Ouvrir la visualization"))
        self.pushButton_6.setText(_translate("Dialog", "Ouvrir la base de données"))
        self.pushButton_2.setText(_translate("Dialog", "Extraire le texte"))
        self.pushButton_3.setText(_translate("Dialog", "Extraire les données"))
        self.pushButton_4.setText(_translate("Dialog", "Écrire la base de données"))
        self.pushButton_7.setText(_translate("Dialog", "Texte -> base de données"))
        self.label_2.setText(_translate("Dialog", "Inscrivez le mot que vous désirez chercher"))
        self.label_3.setText(_translate("Dialog", "Profondeur de la recherche"))
        self.pushButton_8.setText(_translate("Dialog", "Rechercher dans le navigateur Neo4j"))
        self.pushButton_9.setText(_translate("Dialog", "Rechercher dans la visualisation 3D"))
        
    # Start CoreNLP server when opening the app
    def startCoreNLP(self):
        try:
            subprocess.Popen(self.javaCmd, shell=True, cwd='./CoreNLP')
        except Exception as e:
            self.plainTextEdit.appendPlainText("Something went wrong while starting the natural language processing engine... do you have java installed on your path ? \n")
            self.plainTextEdit.appendPlainText("Here are more details : " + str(e) + " \n")

    def startNeo4J(self):
        pass

    # Select the file to process
    def selectFile(self):
        self.filePath = self.parser.selectFile()
        self.plainTextEdit.appendPlainText("You have selected to following file : " + self.filePath + "\n")

    # Parse the file
    def parseFile(self):
        if not self.filePath:
            self.plainTextEdit.appendPlainText("Please choose a file first ! \n")
        else :
            self.text = self.parser.parseFile(self.filePath)
            self.plainTextEdit.appendPlainText("The content of your file has been parsed successfully ! \n")

    def extractContent(self):
        self.csvhandler.clearCSV(self.csvFilePath)
        self.csvhandler.writeToCSV(self.nlppipeline.extractContent(self.nlpServer, self.text), self.csvFilePath)
        self.plainTextEdit.appendPlainText("The ontology of your text has been extracted ! \n")

    def writeDatabase(self):
        self.dbhandler.emptyDB(self.graph, self.emptyDBQuery)
        self.dbhandler.populateDB(self.graph, self.loadCSVQuery)
        self.plainTextEdit.appendPlainText("The concepts and relationships of your database successfully loaded into the database ! \n")

    def textToDB(self):
        self.parseFile()
        self.extractContent()
        self.writeDatabase()

    def openWebGraph(self):
        self.plainTextEdit.appendPlainText("Opening browser visualization...")
        self.webgraph.openWebGraph(self.browserPath, self.graphPath)
        self.plainTextEdit.appendPlainText("Visualization successfully invoked !")

    def openNeo4JBrowser(self):
        self.plainTextEdit.appendPlainText("You might need to enter the following password to access the database : s0mep4ssw0rd \n")
        self.webgraph.openNeo4JBrowser(self.browserPath, self.Neo4JBrowserPath)
        self.plainTextEdit.appendPlainText("Enter the following command in the upper field to map all concepts and relationships : MATCH (n)-[r]->(m) RETURN n,r,m; ")