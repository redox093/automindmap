













































525 UNIVERSITY AVE.  |  PALO ALTO, CA 94301  |  KYNDI.CAOM

Kyndi Patent Pending: Quanton
PREAMBLE

Introduction

Today a tremendous effort is being made around the creation of quantum computers. 

This high-cost, high-stakes effort by physicists is an attempt to harness the secrets of 

quantum physics to conceive a whole new form of computing. One of the reasons for this 

effort is to address very expensive optimization problems that exist across all areas of 

artificial intelligence (AI).

Kyndi has found a way to emulate the results of a real quantum computer for a large 

class of problems relevant to the Kyndi solution, specifically to the creation of ontologies 

and to making inferences over our “knowledge soup.”

Why Is The Innovation Needed?

The field of AI is littered with problems that, if they were fully computed, would require 

years, decades, or centuries to complete. Reasoning over language is precisely 

one of these problems and is one reason why natural language understanding has 

been so elusive. AI can be seen as the science of finding heuristics, shortcuts, and 

representations that negotiate the complexity of problems such as natural language 

understanding.

For Kyndi, developing a way to speed the calculations involved in natural language 

understanding has been one of three key innovations that makes our solution possible. 

With the quantum physics approach, it is possible to perform the equivalent of a trillion 

operations in one computation step for certain types of problems.

What Does It Do? 

A type of quantum computing model called permutation quantum computing looks 



525 UNIVERSITY AVE.  |  PALO ALTO, CA 94301  |  KYNDI.CAOM

at quantum operations over permutations of a set of data. The Quanton is a method 

by which these operations can be performed on a classic GPU-enabled computer, 

specifically when the implied parallelism is within reasonable bounds. 

More technically, the Quanton is a general-purpose approximation model to bounded 

quantum computing, specifically inspired by topological quantum computing (TQC) and 

the standard quantum circuit model. In the Quanton-based TQC, any computation can be 

solely defined by permutations. The Quanton serves as an Approximating Turing Machine 

with computation represented by permutations embedded in a wave function. The 

wave function is modelled by embeddings of directional geometric probability density 

functions into manifolds that represent the data or computation geometry. 

Quanton technology has implications well beyond Kyndi natural language 

understanding. Areas of potential relevance include almost any optimization problem. 

Examples include air traffic control, self-driving cars, social network analysis, cyber 

data analysis, high dimensionality factorially big data analysis, image analysis, and 

more.












































































































































































