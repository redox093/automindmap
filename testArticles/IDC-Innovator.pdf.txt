












































 

December 2018, IDC #US44505718 

IDC Innovators 

IDC Innovators: AI Software Platforms, 2018 

David Schubmehl 

THIS IDC INNOVATORS EXCERPT FEATURES: KYNDI 

IN THIS EXCERPT 

The content for this excerpt was taken directly from IDC Innovators: AI Software Platforms, 2018 (Doc 

#US44505718). 

 
Why Kyndi Was Chosen as an IDC Innovator 
Kyndi is an AI software platform focusing on "explainable AI" and the automatic creation of knowledge 

graphs in order to be able to answer challenging questions from reams of unstructured information. Kyndi 

has seen significant success in working with government agencies and Fortune 500 pharmaceutical 

companies to quickly identify and locate "hidden" information that is extremely useful to knowledge 

workers. 

 
 
 
 
 
 
 
 
 
 
 
 
IDC Innovator Assessment 
• Kyndi has abilities to automatically identify, extract, and connect disparate information entities, 

relationships, and events into usable knowledge graphs that can be searched and exploited for  

general-purpose question answering. 

• Kyndi offers the capability of providing information on how answers and recommendations were arrived 

at by providing a causal chain of application actions and decisions that can be utilized by analysts and 

knowledge workers. 

• Kyndi also has excellent semantic analysis capabilities that help understand and relate key information 

nuggets together regardless of whether they exist in the same document or sets of documents. This 

allows Kyndi to build sophisticated knowledge graphs automatically that are actionable and responsive 

to a wide variety of applications and use cases. 

 

Number of Customers 
<10 

$250K to $1M 

60% United States 
40% EMEA 

Typical Deal Size 

Geographic Mix 
(% of Revenue by Major Region) 

Company Name 

Founded Number of Employees Headquarters 

Product Name 

Profiled Product/ 
Service 

Founders 

Funding 

Kyndi Explainable AI 

2014 

Ryan Welsh, Arun Majumdar 

Kyndi Input Data Services 
Kyndi Cognitive Memory 
Kyndi Cognitive Search 
Kyndi Knowledge Graph 
Visualizer  

28 

$12.5 million 

San Mateo, California 



©2018 IDC #US44505718 2 

Key Differentiator 
Many AI and machine learning models are built on top of frameworks using various types of learning that  

for the most part are considered "black box" (i.e., the rationale for the prediction or recommendation is not 

known). Kyndi's "Explainable AI" works different from these approaches. Kyndi's software can explain how 

and why it provided the answer it did. This visibility allows organizations to have confidence in the 

system's responses and be aware of any potential issues or problems. 

 

Challenges 
As a relatively small start-up, Kyndi's primary challenges revolve around awareness and marketing. While 

explainability is an increasingly important topic in the use of AI software platforms, other larger AI platform 

vendors are starting to message about this, and Kyndi needs to become more visible in the marketplace. 

IDC INNOVATORS IN AI SOFTWARE PLATFORMS 

The market for AI software platforms has exploded over the past several years, fueled by a wide range 

of tools available from start-ups to established vendors in the market such as Google, Microsoft, 

Amazon, and IBM. These platforms are being used to develop AI-enabled applications that provide 

predictions, recommendations, advisory services, and even business process automation. To date, 

there are dozens of vendors seeking to address the needs and desires of organizations racing to 

implement digital transformation via the use of innovative and potentially ground-breaking AI-based 

applications. These suppliers offer a very broad range of capabilities and functionality in developing 

general-purpose AI applications. This document outlines several smaller vendors that offer some of 

the more innovative capabilities available in the market today (see Figures 1–6). 

TECHNOLOGY DEFINITION 

AI software platforms facilitate the development of intelligent, advisory, process automation, and AI-

centric applications. The technology components of AI software platforms include text analytics, rich 

media (such as audio, video, and image) analytics, tagging, searching, machine learning, deep 

learning, categorization, clustering, hypothesis generation, question answering, visualization, filtering, 

alerting, and navigation. These platforms typically include knowledge representation tools such as 

knowledge graphs, triple stores, or other types of NoSQL data stores as well as machine learning tools 

to provide some level of recommendation, prediction, or automation. The platforms also provide for 

knowledge curation and continuous automatic learning based on past experiences. 

IDC INNOVATORS INCLUSION CRITERIA 

An "IDC Innovators" document recognizes emerging vendors chosen by an IDC analyst because they 

offer an innovative new technology or a groundbreaking business model, or both, and were approved 

by the IDC Innovators Review Panel. It is not an exhaustive evaluation of all companies in a segment 

or a comparative ranking of the companies. 

An IDC Innovators document highlights vendors that meet the following criteria: 

▪ In IDC's opinion, the company exhibits innovative technology or a new business model. 

▪ The company has annual revenue <$100 million at the time of selection. 



©2018 IDC #US44505718 3 

▪ Customers are currently using the company's products and services (i.e., the products and 

services are not conceptual or in the process of being released). 

▪ The product, service, or business model must solve or help alleviate an IT buyer challenge. 

In addition, vendors in the process of being acquired by a larger company may be included provided 

the acquisition is not finalized at the time of publication of the document. Vendors funded by venture 

capital firms may also be included even if the venture capital firm has a financial stake in the vendor's 

company. 

LEARN MORE 

Related Research 

▪ IDC FutureScape: Worldwide Analytics and Artificial Intelligence 2019 Predictions (IDC 

#US44389418, October 2018) 

▪ Market Analysis Perspective: Worldwide AI Software Platforms and Components, 2018 (IDC 

#US43583418, September 2018) 

▪ IDC's Worldwide Semiannual Cognitive/Artificial Intelligence Systems Spending Guide 

Taxonomy, 2018: Update (IDC #US44264118, September 2018) 

▪ IDC Innovators: Conversational AI Software Platforms, 2018 (IDC #US44001917, June 2018) 

▪ AI Is Hard: What Does It Take to Make AI Work for You? (IDC #DR2018_T3_DS, February 

2018) 

Synopsis 

IDC Innovators are emerging vendors with revenue <$100 million that have demonstrated either a 

groundbreaking business model or an innovative new technology — or both. This IDC Innovators study 

profiles five vendors that are providing innovative AI software platforms: Arago, Ayasdi, 

CognitiveScale, Kyndi, and Primer. 

"AI-enabled applications are being increasingly used by many types of organizations to provide advice, 

recommendations, predictions, and business process automation," says David Schubmehl, research 

director, Cognitive/Artificial Intelligence Systems at IDC. "Many of these organizations are undertaking 

projects developing these AI-enabled applications based on the use of commercially available AI 

software platforms that contain the technologies needed to quickly create, develop, implement, and 

test these new types of applications." 

 



 

About IDC 

International Data Corporation (IDC) is the premier global provider of market intelligence, advisory 

services, and events for the information technology, telecommunications and consumer technology 

markets. IDC helps IT professionals, business executives, and the investment community make fact-

based decisions on technology purchases and business strategy. More than 1,100 IDC analysts 

provide global, regional, and local expertise on technology and industry opportunities and trends in 

over 110 countries worldwide. For 50 years, IDC has provided strategic insights to help our clients 

achieve their key business objectives. IDC is a subsidiary of IDG, the world's leading technology 

media, research, and events company. 

Global Headquarters 

5 Speen Street 

Framingham, MA 01701  

USA 

508.872.8200 

Twitter: @IDC 

idc-community.com 

www.idc.com 

Copyright and Trademark Notice 

This IDC research document was published as part of an IDC continuous intelligence service, providing written 

research, analyst interactions, telebriefings, and conferences. Visit www.idc.com to learn more about IDC 

subscription and consulting services. To view a list of IDC offices worldwide, visit www.idc.com/offices. Please 

contact the IDC Hotline at 800.343.4952, ext. 7988 (or +1.508.988.7988) or sales@idc.com for information on 

applying the price of this document toward the purchase of an IDC service or for information on additional copies 

or web rights. IDC Innovator and IDC Innovators are trademarks of International Data Group, Inc. 

Copyright 2018 IDC. Reproduction is forbidden unless authorized. All rights reserved. 


