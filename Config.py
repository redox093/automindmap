# -*- coding: utf-8 -*-

class Config:

    _instance = {}

    nlpServerHttp = "http://localhost:9000/"
        
    neo4jbolt = 'bolt://localhost:7687'
    neo4jhttp = 'http://localhost:7474'
    neo4juser = 'neo4j'
    neo4jpw = 's0mep4ssw0rd'

    csvDumpPath = "csvDump.csv"
    csvFolderPath = "csvFolder/"

    textFilesFolderPath = "textFolder/"

    graphPath = 'WebGraph/graph.html'
    neo4jBrowserPath = 'http://localhost:7474/browser/'

    # Chrome Path because webbrowser opens IE by default... -.-
    browserPath = 'C:/Program Files (x86)/Google/Chrome/Application/chrome.exe %s'

    def __init__(self):
        self.__dict__ = self._instance

    def restoreDefaults(self):
        self.nlpServerHttp = "http://localhost:9000/"
        self.neo4jbolt = 'bolt://localhost:7687'
        self.neo4jhttp = 'http://localhost:7474'
        self.neo4juser = 'neo4j'
        self.neo4jpw = 's0mep4ssw0rd'
        self.csvDumpPath = "csvDump.csv"
        self.graphPath = 'WebGraph/graph.html'
        self.neo4jBrowserPath = 'http://localhost:7474/browser/'
        # Chrome Path because webbrowser opens IE by default... -.-
        self.browserPath = 'C:/Program Files (x86)/Google/Chrome/Application/chrome.exe %s'

    def setNLPServerHttp(self, http):
        self.nlpServerHttp = http

    def getNLPServerHttp(self):
        return self.nlpServerHttp

    def setNeo4jServerBolt(self, bolt):
        self.neo4jbolt = bolt

    def getNeo4jServerBolt(self):
        return self.neo4jbolt

    def setNeo4jServerHttp(self, http):
        self.neo4jhttp = http

    def getNeo4jServerHttp(self):
        return self.neo4jhttp

    def setNeo4jUsername(self, name):
        self.neo4juser = name

    def getNeo4jUsername(self):
        return self.neo4juser

    def setNeo4jPassword(self, pw):
        self.neo4jpw = pw
    
    def getNeo4jPassword(self):
        return self.neo4jpw

    def setcsvDumpPath(self, path):
        self.csvDumpPath = path

    def getcsvDumpPath(self):
        return self.csvDumpPath

    def setcsvFolderPath(self, path):
        self.csvFolderPath = path

    def getcsvFolderPath(self):
        return self.csvFolderPath

    def setTextFolderPath(self, path):
        self.textFilesFolderPath = path

    def getTextFolderPath(self):
        return self.textFilesFolderPath

    def setNeo4jBrowserPath(self, path):
        self.neo4jBrowserPath = path

    def getNeo4jBrowserPath(self):
        return self.neo4jBrowserPath

    def setWebGraphPath(self, path):
        self.graphPath = path

    def getWebGraphPath(self):
        return self.graphPath

    def setBrowserPath(self, path):
        self.browserPath = path

    def getBrowserPath(self):
        return self.browserPath